select fm.value, count(fm.value) as count from metadata_key mk 
inner join file_metadata fm on mk.id = fm.key_id
inner join file_header fh on fh.id = fm.file_id
where mk.key_name = 'Content-Type' and fh.batch_id = 1
group by fm.value order by count desc;
