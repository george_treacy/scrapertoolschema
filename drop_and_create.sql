# recreate tables

drop table if exists redeye.file_metadata;
drop table if exists redeye.metadata_key;
drop table if exists redeye.file_header;

CREATE TABLE file_header (
     id MEDIUMINT NOT NULL AUTO_INCREMENT,
     batch_id char(36),
     size_bytes BIGINT UNSIGNED NOT NULL,
     sha_256 CHAR(65),
     path VARCHAR(256),
     fname VARCHAR(256),
     PRIMARY KEY (id, batch_id)
) ENGINE=InnoDB;

CREATE TABLE metadata_key (
     id MEDIUMINT NOT NULL AUTO_INCREMENT,
     key_name VARCHAR(256),
     PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE file_metadata (
     file_id MEDIUMINT NOT NULL,
	 key_id MEDIUMINT NOT NULL,
     value VARCHAR(256),
     PRIMARY KEY (file_id, key_id),
     FOREIGN KEY (file_id) REFERENCES file_header(id),
     FOREIGN KEY (key_id) REFERENCES metadata_key(id)
) ENGINE=InnoDB;
