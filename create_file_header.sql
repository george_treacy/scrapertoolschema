CREATE TABLE file_header (
     etag CHAR(255),
	 bucket VARCHAR(1024),
     keey VARCHAR(1024),
     PRIMARY KEY (etag)
) ENGINE=InnoDB;

CREATE TABLE file_duplicate (
     etag CHAR(255),
     path_md5 CHAR(64),
	 bucket VARCHAR(1024),
     keey VARCHAR(1024),
     PRIMARY KEY (etag, path_md5)
) ENGINE=InnoDB;