CREATE TABLE metadata_key (
     id MEDIUMINT NOT NULL AUTO_INCREMENT,
     key_name VARCHAR(256),
     PRIMARY KEY (id)
) ENGINE=InnoDB;