insert into file_header (batch_id, size_bytes, sha_256, path, fname) values(1, 100, 
'c3ab8ff13720e8ad9047dd39466b3c8974e592c2fa383d4a3960714caef0c4f2', 
'georgeit/images/', 'poo.png');

insert into metadata_key (key_name) values ('Creator');
insert into metadata_key (key_name) values ('Author');
insert into metadata_key (key_name) values ('FileType');
insert into metadata_key (key_name) values ('CreatedDate');
insert into metadata_key (key_name) values ('Zingerdoodle');

insert into file_metadata (file_id, key_id, value) values (1, 1, 'George Treacy'); 
insert into file_metadata (file_id, key_id, value) values (1, 2, 'Sam Clemens'); 
insert into file_metadata (file_id, key_id, value) values (1, 3, '09/11/2001'); 
insert into file_metadata (file_id, key_id, value) values (1, 4, 'Boshzbloshker'); 