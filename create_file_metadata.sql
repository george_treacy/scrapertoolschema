CREATE TABLE file_metadata (
     file_id MEDIUMINT NOT NULL,
	 key_id MEDIUMINT NOT NULL,
     value VARCHAR(256),
     PRIMARY KEY (file_id, key_id),
     FOREIGN KEY (file_id) REFERENCES file_header(id),
     FOREIGN KEY (key_id) REFERENCES metadata_key(id)
) ENGINE=InnoDB;