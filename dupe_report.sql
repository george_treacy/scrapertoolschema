select id, count(*) as NumDuplicates
from file_header where batch_id = 1

group by sha_256
having count(*) > 1